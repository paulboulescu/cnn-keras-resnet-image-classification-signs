# CNN (ResNet) Image Classifier with Keras

## About
Convolutional Neural Network model with Keras, based on ResNet architecture, used to classify images of 6 hand signs, corresponding to numbers from 0 to 5.

## Instalation
1. Clone the repository
```
$ git clone https://paulboulescu@bitbucket.org/paulboulescu/cnn-keras-resnet-image-classification-signs.git
```

2. Train and Test
```
$ python model.py
```

## Requirements
1. Keras

## Details
* Model: ResNet (50 layers)
* Train Accuracy: **0.9324**
* Test Accuracy: **0.8916**
* Epochs: **20**
* MiniBatch: **32**
* Learning Rate: **0.001**
* Optimizer: **Adam**
* Initializer: **Xavier**
* Loss: **Softmax / Cross Entropy**

## Disclaimer
Created under the Deep Learning Specialization (Andrew Ng, Younes Bensouda Mourri, Kian Katanforoosh) - Coursera. The code uses ideas, datasets, algorithms, and code fragments presented in the Course.